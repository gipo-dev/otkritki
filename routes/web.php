<?php

use Spatie\Sitemap\SitemapGenerator;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'api'], function () {
    Route::get('/', 'WebsiteController@indexApi')->name('api.index');
    Route::get('category/{category}', 'CategoryController@showApi')->name('api.category.show');
//    Route::get('postcard/{postcard}/category/{category?}', 'PostcardController@showCategoryApi')->name('api.postcard.show');
//    Route::get('feasts', 'CategoryController@feastsApi')->name('api.feasts.list');
    Route::get('kalendar-prazdnikov', 'CategoryController@feastsApi')->name('api.feasts.list');
    Route::get('kalendar-prazdnikov/{month}', 'CategoryController@feastApi')->name('api.feasts.show');
    Route::get('catalog', 'CategoryController@catalogApi')->name('api.catalog.list');
    Route::get('info/{name}', 'WebsiteController@infoApi')->name('api.info.page');
    Route::group([ 'middleware' => ['isCategory'] ], function() {
        Route::get('{category}/{postcardOrCategory?}/{postcard?}', 'CategoryController@showApi')->name('api.category.show');
    });
});

Route::get('sitemap/generate', 'WebsiteController@generateSitemap')->name('sitemap.generate');

Route::group(['middleware' => ['auth', 'isModer'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/', 'Admin\AdminController@index')->name('index');
    Route::resource('categories', 'Admin\CategoryController');
    Route::resource('category-backgrounds', 'Admin\CategoryBackgroundController');
    Route::resource('category-backgrounds-categories', 'Admin\CategoryBackgroundCategoryController');
    Route::resource('postcards', 'Admin\PostcardController');
    Route::resource('feasts', 'Admin\FeastController');
    Route::group(['prefix' => 'parser', 'as' => 'parser.'], function () {
        Route::get('/', 'Admin\ParserController@index')->name('index');
        Route::get('/make', 'Admin\ParserController@make')->name('make');
        Route::get('/categories', 'Admin\ParserController@categories')->name('categories');
        Route::get('/create-postcard', 'Admin\ParserController@createPostcard')->name('createPostcard');
        Route::get('/delete-postcard', 'Admin\ParserController@deletePostcard')->name('deletePostcard');
    });
    Route::group(['middleware' => ['auth', 'isAdmin']], function () {
        Route::resource('users', 'Admin\UserController');
        Route::resource('info-pages', 'Admin\InfoPageController');
    });
});

Auth::routes();

Route::get('/', 'WebsiteController@index')->name('index');
Route::get('kalendar-prazdnikov', 'CategoryController@feasts')->name('feasts.list');
Route::get('kalendar-prazdnikov/{month}', 'CategoryController@feast')->name('feasts.show');
Route::get('catalog', 'CategoryController@catalog')->name('catalog.list');
Route::get('info/{name}', 'WebsiteController@info')->name('info.page');
Route::group([ 'middleware' => ['isCategory'] ], function() {
    Route::get('{category}/{postcardOrCategory?}/{postcard?}', 'CategoryController@show')->name('category.show');
});
