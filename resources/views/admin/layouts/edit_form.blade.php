@extends('admin.layouts.app')

@section('content')
    <h3 class="mb-3">
        Редактирование {{ $title ?? '' }}
        <div class="float-right">
            @isset($delete)
                <form action="{{ $delete }}" method="POST">
                    @csrf
                    {{ method_field('delete') }}
                    <button type="submit" class="btn btn-danger">
                        <i class="fa fa-trash"></i>
                    </button>
                </form>
            @endisset
        </div>
    </h3>
    <form action="{{ $form_route }}" enctype="multipart/form-data" method="POST" class="pb-5">
        @csrf
        {{ method_field($method) }}
        @foreach($form as $i => $element)
            <div class="form-group">
                <label
                    for="{{ $element['name'].'_'.$i }}">{{ $element['text'] }}{{ isset($element['required']) && $element['required'] ? '*' : '' }}
                    :</label>
                @if($element['type'] == 'text')
                    <input type="text" class="form-control" id="{{ $element['name'].'_'.$i }}"
                           name="{{ $element['name'] }}"
                           value="{{ $element['value'] ?? '' }}" {{ isset($element['required']) && $element['required'] ? 'required' : '' }}>
                @elseif($element['type'] == 'select')
                    <select class="custom-select" id="{{ $element['name'].'_'.$i }}"
                            name="{{ $element['name'] }}"
                            value="{{ $element['value'] ?? '' }}" {{ isset($element['required']) && $element['required'] ? 'required' : '' }}>
                        <option value>-- Выбрать --</option>
                        @foreach($element['items'] as $k => $val)
                            <option value="{{ $k }}" {{ $val->id == $element['value'] ? 'selected' : '' }}>{{ $val->name
                                ?? '' }}
                            </option>
                        @endforeach
                    </select>
                @elseif($element['type'] == 'image')
                    @isset($element['value'])
                        <div>
                            <img src="{{ $element['value'] }}" alt="..." class="img-thumbnail">
                        </div>
                        <div>
                            <input type="file" id="{{ $element['name'].'_'.$i }}"
                                   accept="image/jpeg,image/png,image/gif"
                                   name="{{ $element['name'] }}"
                                   value="{{ $element['value'] ?? '' }}">
                        </div>
                    @else
                        <div>
                            <input type="file" id="{{ $element['name'].'_'.$i }}"
                                   accept="image/jpeg,image/png,image/gif"
                                   name="{{ $element['name'] }}"
                                   value="{{ $element['value'] ?? '' }}"
                                {{ isset($element['required']) && $element['required'] ? 'required' : '' }}>
                        </div>
                    @endisset
                @elseif($element['type'] == 'textarea')
                    <textarea class="form-control" name="{{ $element['name'] }}"
                              {{ isset($element['required']) && $element['required'] ? 'required' : '' }} rows="3">{{ $element['value'] ?? '' }}</textarea>
                @elseif($element['type'] == 'text-editable')
                    <textarea class="summernote" name="{{ $element['name'] }}"
                              {{ isset($element['required']) && $element['required'] ? 'required' : '' }} rows="3">{{ $element['value'] ?? '' }}</textarea>
                @elseif($element['type'] == 'color')
                    <input type="color" id="{{ $element['name'].'_'.$i }}"
                           accept="image/jpeg,image/png,image/gif"
                           name="{{ $element['name'] }}"
                           value="{{ $element['value'] ?? '' }}"
                        {{ isset($element['required']) && $element['required'] ? 'required' : '' }}>
                @elseif($element['type'] == 'select')
                    <select class="form-control"
                            id="{{ $element['name'].'_'.$i }}"
                            name="{{ $element['name'] }}"
                        {{ isset($element['required']) && $element['required'] ? 'required' : '' }}>
                        @foreach($element['items'] as $k => $val)
                            <option value="{{ $k }}" {{ isset($element['value'][$val->id]) ? 'selected' : '' }}>
                                @isset($element['name_key'])
                                    {{ $val->{$element['name_key']} ?? '' }}
                                @else
                                    {{ $val->name ?? '' }}
                                @endisset
                            </option>
                        @endforeach
                    </select>
                @elseif($element['type'] == 'select-multiple')
                    <select class="form-control multi-select" multiple="multiple" style="display: none;"
                            id="{{ $element['name'].'_'.$i }}"
                            name="{{ $element['name'] }}"
                        {{ isset($element['required']) && $element['required'] ? 'required' : '' }}>
                        @foreach($element['items'] as $k => $val)
                            <option value="{{ $k }}" {{ isset($element['value'][$val->id]) ? 'selected' : '' }}>
                                @isset($element['name_key'])
                                    {{ $val->{$element['name_key']} ?? '' }}
                                @else
                                    {{ $val->name ?? '' }}
                                @endisset
                            </option>
                        @endforeach
                    </select>
                @elseif($element['type'] == 'date')
                    <input type="date" class="form-control" id="{{ $element['name'].'_'.$i }}"
                           name="{{ $element['name'] }}"
                           value="{{ $element['value'] ?? '' }}"
                        {{ isset($element['required']) && $element['required'] ? 'required' : '' }}>
                @endif
            </div>
        @endforeach
        <button class="btn btn-success"><i class="fa fa-save"></i> Сохранить</button>
    </form>
@endsection

@push('styles')
    <style>
        .img-thumbnail {
            max-width: 300px;
        }
    </style>
@endpush
@push('scripts')
    <script>
    </script>
@endpush
