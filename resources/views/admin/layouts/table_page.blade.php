@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.table', ['table' => $table])
@endsection

@push('styles')
    <style>
        th:last-child,
        td:last-child {
            text-align: right;
        }
    </style>
@endpush
