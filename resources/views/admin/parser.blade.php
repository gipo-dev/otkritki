@extends('admin.layouts.app')

@section('content')
    <div id="vue">
        @verbatim
            <div class="form-group">
                <label for="exampleInputEmail1">Адрес сайта:</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="https://website.com"
                       v-model:value="url" :disabled="states.loading">
                <button @click.prevent="getWebsiteData()" type="button" class="btn btn-primary mb-2 mt-2"
                        :disabled="states.loading">Начать парсинг
                </button>
            </div>
            <div class="form-group">
                <label>Категории для добавления:</label>
                <select class="form-control multi-categories" multiple="multiple"
                        id="u_categories" style="display: none">
                    @endverbatim
                    @foreach($categories as $c)
                        <option value="{{ $c->id }}">{{ $c->name }}<-{{ $c->parent->name ?? 'Праздники' }}</option>
                    @endforeach
                    @verbatim
                </select>
            </div>
            <table class="table" v-if="page != null">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название</th>
                    <th scope="col">Действие</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(r, i) in page">
                    <td>
                        <img :src="r.src" :alt="r.alt" style="max-width: 200px;max-height: 200px">
                    </td>
                    <td>
                        <input type="text" class="form-control" v-model="r.alt">
                        <input type="text" class="form-control" v-model="r.src">
                    </td>
                    <td>
                        <button v-if="r.id == -1" class="btn btn-primary" @click="createPostcard(r, i)">Сохранить</button>
                        <button v-if="r.id != -1" class="btn btn-danger" @click="deletePostcard(r, i)">Удалить</button>
                    </td>
                </tr>
                </tbody>
            </table>
        @endverbatim
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script>
        var app = new Vue({
            el: '#vue',
            data: function () {
                return {
                    url: 'https://otkritkiok.ru/pojelanie/936939075258?utm=category2',
                    page: null,
                    selectedCategories: [],
                    states: {
                        loading: false,
                    },
                    categories: [],
                }
            },
            methods: {
                getCategories: function () {
                    var that = this;
                    that.states.loading = true;
                    axios({
                        method: 'get',
                        url: '{{ route('admin.parser.categories') }}'
                    })
                        .then(function (response) {
                            that.categories = response.data;
                            $(".multi-categories").dashboardCodeBsMultiSelect();
                        })
                        .catch(function (error) {
                            alert(error);
                        })
                        .finally(function () {
                            that.states.loading = false;
                        })
                },
                getWebsiteData: function () {
                    var that = this;
                    that.states.loading = true;
                    axios({
                        method: 'get',
                        url: '{{ route('admin.parser.make') }}',
                        params: {
                            website_url: that.url,
                        }
                    })
                        .then(function (response) {
                            that.page = response.data;
                        })
                        .catch(function (error) {
                            alert(error);
                        })
                        .finally(function () {
                            that.states.loading = false;
                        })
                },
                createPostcard: function (elem, i) {
                    var that = this;
                    var categroies = $('#u_categories').val();
                    axios({
                        method: 'get',
                        url: '{{ route('admin.parser.createPostcard') }}',
                        params: {
                            name: elem.alt,
                            path: elem.src,
                            categories: categroies,
                        }
                    })
                        .then(function (response) {
                            that.page[i].id = response.data.id;
                        })
                        .catch(function (error) {
                            alert(error);
                        })
                        .finally(function () {
                            that.states.loading = false;
                        })
                },
                deletePostcard: function (elem, i) {
                    var that = this;
                    var categroies = $('#u_categories').val();
                    axios({
                        method: 'get',
                        url: '{{ route('admin.parser.deletePostcard') }}',
                        params: {
                            postcard_id: elem.id,
                        }
                    })
                        .then(function (response) {
                            that.page[i].id = -1;
                        })
                        .catch(function (error) {
                            alert(error);
                        })
                        .finally(function () {
                            that.states.loading = false;
                        })
                }
            },
            mounted: function () {
                this.getCategories();
            }
        });
    </script>
@endpush
