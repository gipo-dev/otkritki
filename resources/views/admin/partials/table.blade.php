<h2>Список {{ $title ?? '' }}
    <div class="float-right">
        @isset($create)
            <a href="{{ $create }}" class="btn btn-success">
                <i class="fa fa-plus"></i>
            </a>
        @endisset
    </div>
</h2>
<table class="table table-striped">
    <thead>
    <tr>
        @foreach($table['columns'] as $col)
            <th scope="col">{{ $col }}</th>
        @endforeach
        <th scope="col">Действие</th>
    </tr>
    </thead>
    <tbody>
    @foreach($table['rows'] as $row)
        <tr>
            @foreach($row['values'] as $elem)
                <th>{{ $elem }}</th>
            @endforeach
            <th>
                <a href="{{ $row['link'] }}" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
            </th>
        </tr>
    @endforeach
    </tbody>
</table>
{{ $pagination->links() }}
