@extends('layouts.app')

@section('title', $page->meta_title)
@section('description', $page->meta_description)

@section('content')
    <h1>{{ $page->title }}</h1>
    {!! $page->body !!}
@endsection
