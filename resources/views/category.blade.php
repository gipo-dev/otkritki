@extends('layouts.app')

@section('title', $category->meta_title)
@section('description', $category->meta_description)

@section('content')
    <nav aria-label="breadcrumb" id="is-category">
        <ol class="breadcrumb mb-2">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            @if($category->parent != null)
                <li class="breadcrumb-item"><a
                        href="{{ route('category.show', [$category->parent->slug ?? '']) }}">{{ $category->parent->name ?? '' }}</a>
                </li>
            @endif
            <li class="breadcrumb-item active" aria-current="page">{{ $category->name }}</li>
        </ol>
    </nav>
    <div class="card mb-2 bg-primary top-menu">
        <div class="card-body">
            <h1 class="h4 mb-0 text-center font-weight-bolder text-white">{{ $category->name }}</h1>
        </div>
    </div>
    @isset($category->parent->childs)
        <div class="mb-2 owl-carousel">
            @foreach($category->parent->childs as $link)
                <a href="{{ route('category.show', [ $category->parent->slug, $link->slug ]) }}"
                   class="badge badge-pill badge-primary px-2 py-1">{{ $link->name }}</a>
            @endforeach
        </div>
    @endisset
    @if($category->description != null &&$category->description != '')
        <div class="card mb-2">
            <div class="card-body pb-0">
                <div class="row">
                    <div class="col-12 category-description" id="category-description">
                        {!! $category->description !!}
                    </div>
                    @if(strlen($category->description) > 246)
                        <div class="col-12 py-1">
                            <a href="#" id="show-more">Показать больше</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    @endif
    <div class="card mb-2">
        <div class="card-body">
            <div class="row" id="postcards-container">
                @foreach($postcards as $postcard)
                    @include('partials.postcard_item', ['postcard' => $postcard])
                @endforeach
            </div>
        </div>
    </div>
    @if($postcards->lastPage() > 1)
        <div class="card-body d-flex justify-content-center">
            <div class="buttons__order buttons__btn_position buttons__order_download" data-name="download">
                <div class="buttons__btn buttons__btn_download">
                    <div class="buttons__right py-2" id="loadmore" data-maxpage="{{ $postcards->lastPage() }}">загрузить
                        ещё
                    </div>
                </div>
            </div>
        </div>
    @endif
    {{--        {{ $postcards->links() }}--}}
@endsection

@push('styles')
    <style>
        .category-description {
            max-height: 70px;
            overflow: hidden;
            transition: all .2s;
        }

        .category-description.open {
            max-height: unset;
        }
    </style>
@endpush
