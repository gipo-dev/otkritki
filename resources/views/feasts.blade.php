@extends('layouts.app')

@section('title', 'Открытки на праздники')
@section('description', 'Открытки на праздники')

@section('content')
    <div class="card mb-3 bg-primary top-menu">
        <div class="card-body">
            <h1 class="h4 mb-0 text-center font-weight-bolder text-white">Календарь праздников</h1>
        </div>
    </div>

    <div class="mb-3 slider-wrap">
        <div class="owl-carousel">
            <a href="{{ route('feasts.list') }}"
               class="badge badge-pill badge-primary px-4 py-2">Все</a>
            @foreach($mlist as $month)
                <a href="{{ route('feasts.show', $month['slug']) }}"
                   class="badge badge-pill badge-primary px-4 py-2">{{ $month['name'] }}</a>
            @endforeach
        </div>
    </div>

    <ul class="list-unstyled">
        @php $y = 0 @endphp
        @foreach($day_group as $k => $group)
            <div class="card mb-2 bg-info top-menu">
                <div class="card-body">
                    <h3 class="h4 mb-0 text-center font-weight-bolder">{{ $months[$k] ?? '' }}</h3>
                </div>
            </div>
            <ul class="list-unstyled">
                @foreach($group as $i => $date)
                    @php $y++ @endphp
                    <li class="d-flex mb-2">
                        <span class="badge badge-{{ $y == 1 ? 'primary' : 'info' }} px-2 py-1 mr-1">
                            <p class="text-center h3 m-0">{{ $date->day->format('d') }}</p>
                            {{ str_limit($date->day->monthName, 3, '') }}
                        </span>
                        <ul class="list-unstyled flex-grow-1 days">
                            @if($date->feasts != null)
                                @foreach($date->feasts as $feast)
                                    <li>
                                        <span class="badge badge-{{ $k == 0 ? 'primary' : 'info' }} px-2 py-1 w-100">
                                            <a href="{{ route('category.show', $feast->feasts->slug) }}">
                                                {{ $feast->feasts->name ?? $date->day->dayName }}
                                            </a>
                                        </span>
                                    </li>
                                @endforeach
                            @else
                                <li>
                                    <span class="badge badge-info px-2 py-1 w-100">
                                        {{ $date->day->dayName }}
                                    </span>
                                </li>
                            @endif
                        </ul>
                    </li>
                @endforeach
            </ul>
        @endforeach
    </ul>
@endsection

@push('styles')
    <style>
        .days .badge {
            min-height: 40px;
            text-align: left;
            font-size: 17px;
            line-height: 40px;
        }

        .days .badge:first-letter {
            text-transform: uppercase;
        }

        .bg-info, .badge-info {
            background-color: #ececec !important;
            border-bottom: 3px solid #c7c7c8;
        }

        .bg-info .card-body {
            background-color: #ececec;
            border-color: #c7c7c8;
            padding: .3rem;
        }

        .bg-info .h4 {
            text-shadow: none;
        }

        .bg-info .h4:first-letter {
            text-transform: uppercase;
        }
    </style>
@endpush

