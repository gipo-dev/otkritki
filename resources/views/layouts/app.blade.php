<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', 'Тематические открытки')</title>
    <meta name="description" content="@yield('description', 'Тематические открытки')">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
{{--    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">--}}

<!-- Styles -->
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    @stack('styles')
</head>
<body>
<div id="app" style="background-image: url(@yield('background-image'))">
    @include('layouts.top-menu')

    <main class="py-4">
        <div class="container">
            <div class="row">
                <div class="jquery-accordion-menu col-md-3 collapse" id="navbarSupportedContent">
                    <div class="jquery-accordion-menu-header">Тематические открытки</div>
                    <ul class="l-menu-feasts">
                        @foreach(\App\Http\Controllers\WebsiteController::getFeasts() as $i => $date)
                            <li class="{{ $i == 0 ? 'current' : '' }}">
                                <span class="badge badge-pill badge-primary px-2 py-1">
                                    {{ $date->day->format('d.m') }}
                                </span>
                                <ul>
                                    @if($date->feasts != null)
                                        @foreach($date->feasts as $feast)
                                            <li>
                                                <a href="{{ route('category.show', $feast->feasts->slug) }}">
                                                    {{ $feast->feasts->name ?? $date->day->dayName }}
                                                </a>
                                            </li>
                                        @endforeach
                                    @else
                                        <li>
                                            {{ $date->day->dayName }}
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        @endforeach
                        <li>
                            <a href="{{ route('feasts.list') }}" class="d-flex justify-content-center">
                                <span class="badge badge-pill badge-primary px-2 py-1">
                                    Смотреть все праздники
                                </span>
                            </a>
                        </li>
                        <li class="btn-more">
                            <span class="badge badge-pill badge-primary px-2 py-1">
                                Показать больше
                            </span>
                        </li>
                    </ul>
                    <div id="jquery-accordion-menu">
                        <ul>
                            @php $categories = \App\Category::categoryTree() @endphp
                            @foreach($categories[''] as $k => $category)
                                @isset($categories[$category->id])
                                    <li><a href="#"><i
                                                style="background-image: url({{ $category->icon }});
                                                    background-color: {{ $category->color }}"></i>{{ $category->name }}
                                            <span class="more">+</span>
                                        </a>
                                        <ul class="submenu">
                                            @foreach($categories[$category->id] as $sub)
                                                <li>
                                                    <a href="{{ route('category.show', [$category->slug, $sub->slug]) }}">{{ $sub->name }} </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @else
                                    <li>
                                        <a href="{{ route('category.show', $category->slug) }}">
                                            <i style="background-image: url({{ $category->icon }}); background-color: {{ $category->color }}"></i>{{ $category->name }}
                                        </a>
                                    </li>
                                @endisset
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-md-9">
                    @yield('content')
                </div>
            </div>
        </div>
    </main>
</div>
</body>
@stack('scripts')
</html>

@include('layouts.footer')
