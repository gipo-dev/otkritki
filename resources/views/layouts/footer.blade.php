<section class="bg-dark">
    <div class="container">
        <div class="row text-center text-xs-center text-sm-left text-md-left py-4">
            <div class="col-6">
                <a class="navbar-brand text-white" href="/">otkritki.org</a>
            </div>
            <div class="col-6 text-right">
                <a href="{{ route('info.page', 'reglament') }}" class="mr-2 text-white">Регламент</a>
                <a href="{{ route('info.page', 'contacts') }}" class="mr-2 text-white">Контакты</a>
                <a href="{{ route('info.page', 'help') }}" class="text-white">Помощь</a>
            </div>
        </div>
        <div class="row pb-3">
            <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
                <p class="h6">Все права защещены. &copyotkritki.org</p>
            </div>
            </hr>
        </div>
    </div>
</section>
