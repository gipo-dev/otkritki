<div class="bg-primary container-fluid top-menu">
    <nav class="container navbar navbar-expand-lg navbar-dark">
        <a class="navbar-brand" href="/">otkritki.org</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link active" href="{{ route('feasts.list') }}">
                        <i class="far fa-calendar-alt"></i>
                        Праздники <span class="sr-only">(current)</span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>
