<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', 'Тематические открытки')</title>
    <meta name="description" content="@yield('description', 'Тематические открытки')">
    <meta property="og:locale" content="ru_RU" class="next-head">
    <meta property="og:site_name" content="Открытки бесплатно" class="next-head">
    <meta property="og:type" content="website" class="next-head">
    <meta property="og:image" content="@yield('url_image', '')" class="next-head">
    <meta property="og:image:width" content="200" class="next-head">
    <meta property="og:image:height" content="200" class="next-head">
    <meta property="og:image:url" content="@yield('url_image', '')" class="next-head">
    <meta property="og:description" content="НАЖМИТЕ здесь, чтобы открыть" class="next-head">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

<!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
{{--    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">--}}

<!-- Styles -->
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    @stack('styles')
</head>
<body>
<div id="app" class="page-background"
     style="background-image: url(@yield('bg', ''))"
>
    @include('layouts.top-menu')

    <main class="py-4">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @yield('content')
                </div>
            </div>
        </div>
    </main>

    @include('layouts.footer');
</div>
</body>
@stack('scripts')
</html>
