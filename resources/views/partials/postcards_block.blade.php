@foreach($postcards as $postcard)
    @include('partials.postcard_item', ['postcard' => $postcard, 'play' => true])
@endforeach
