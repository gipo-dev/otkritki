<div class="col-lg-4 col-6 postcard px-sm-3 px-1">
    <a href="{{ $postcard->link }}">
        @if(isset($play) && $play == true)
            <div class="image lazy" style="background-image:url({{ $postcard->image ?? '' }});"></div>
        @else
            <div class="image lazy" style="background-image:url({{ $postcard->thumb_image ?? '' }});"
                 data-src="{{ $postcard->image ?? '' }}"></div>
        @endif
        <p class="text-center text-dark text mt-1 px-3">{{ $postcard->name }}</p>
    </a>
</div>
