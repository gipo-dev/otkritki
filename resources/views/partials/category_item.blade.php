<div class="postcard shadow category-item">
    <a href="{{ route('category.show', [$category->parent->slug ?? '', $category->slug]) }}">
        <div class="image lazy" style="background-image: url({{ $category->image ?? '' }})"></div>
        <p class="text-center mb-0">{{ $category->name }}</p>
    </a>
</div>
