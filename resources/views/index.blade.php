@extends('layouts.app')

@section('content')
    <div class="card mb-2 bg-primary top-menu">
        <div class="card-body">
            <h3 class="h4 mb-0 text-center font-weight-bolder text-white">Популярные категории</h3>
        </div>
    </div>
    <div class="mb-3 owl-carousel">
        @foreach($categories as $category)
            @include('partials.category_item', ['category' => $category])
        @endforeach
    </div>
    <div class="card mb-2 bg-primary top-menu w-100">
        <div class="card-body">
            <h1 class="h4 mb-0 text-center font-weight-bolder text-white">Открытки бесплатно</h1>
        </div>
    </div>
    <div class="card mb-2">
        <div class="card-body">
            <div class="row">
                @foreach($postcards as $postcard)
                    @include('partials.postcard_item', ['postcard' => $postcard])
                @endforeach
            </div>
        </div>
    </div>
{{--    <div class="card-body d-flex justify-content-center">--}}
{{--        <div class="buttons__order buttons__btn_position buttons__order_download" data-name="download">--}}
{{--            <div class="buttons__btn buttons__btn_download">--}}
{{--                <div class="buttons__right">смотреть ещё</div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
@endsection
