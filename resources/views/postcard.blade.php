@extends('layouts.app_without_menu')

@section('title', $postcard->name)
@section('description', $category->meta_description ?? '')
@section('bg', $background)
@section('url_image', request()->getSchemeAndHttpHost().$postcard->thumb_image)

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mb-2">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            @if($category != null && $category->parent != null)
                <li class="breadcrumb-item" aria-current="page">
                    <a href="{{ route('category.show', [$category->parent->slug ?? '']) }}">
                        {{ $category->parent->name }}</a>
                </li>
            @endif
            @if($category != null)
                <li class="breadcrumb-item" aria-current="page">
                    <a href="{{ route('category.show', [$category->parent->slug ?? '', $category->slug]) }}">
                        {{ $category->name }}</a>
                </li>
            @endif
        </ol>
    </nav>
    <div class="card mb-2 bg-primary top-menu">
        <div class="card-body">
            <h1 class="h3 mb-0 text-center font-weight-bolder text-white">{{ $postcard->name }}
                <div class="float-right" style="opacity: .7;font-size: 10px;line-height: 30px;">
                    <i class="fa fa-eye"></i>
                    {{ $popularity ?? '1' }}
                </div>
            </h1>
        </div>
    </div>
    <div class="card mb-2">
        <div class="card-body row">
            <div class="col-1 p-0 d-flex justify-content-end justify-content-sm-center align-items-center">
                @isset($prev_next[1])
                    <a href="{{ $prev_next[1]->link }}"
                       class="fa fa-chevron-left text-black-50 prevNext"></a>
                @endisset
            </div>
            <div class="col-10">
                <img src="{{ $postcard->path }}" alt="{{ $postcard->path }}" class="image w-100">
            </div>
            <div class="col-1 p-0 d-flex justify-content-start justify-content-sm-center align-items-center">
                @isset($prev_next[0])
                    <a href="{{ $prev_next[0]->link }}"
                       class="fa fa-chevron-right text-black-50 prevNext"></a>
                @endisset
            </div>
        </div>
    </div>
    <div class="card mb-2">
        <div class="card-body d-flex justify-content-center">
            <div class="buttons__order buttons__btn_position buttons__order_download d-none d-lg-block">
                <a href="{{ $postcard->path }}" download="{{ $postcard->name }}">
                    <div class="buttons__btn buttons__btn_download">
                        <div class="buttons__left">
                            <div class="buttons__icon buttons__icon_padding">
                                <svg viewBox="0 0 475.078 475.077">
                                    <path
                                        d="M467.083,318.627c-5.324-5.328-11.8-7.994-19.41-7.994H315.195l-38.828,38.827c-11.04,10.657-23.982, 15.988-38.828,15.988 c-14.843,0-27.789-5.324-38.828-15.988l-38.543-38.827H27.408c-7.612,0-14.083,2.669-19.414,7.994 C2.664,323.955,0,330.427,0,338.044v91.358c0,7.614,2.664,14.085,7.994,19.414c5.33,5.328,11.801,7.99,19.414,7.99h420.266 c7.61,0,14.086-2.662,19.41-7.99c5.332-5.329,7.994-11.8,7.994-19.414v-91.358C475.078,330.427,472.416,323.955,467.083,318.627z M360.025,414.841c-3.621,3.617-7.905,5.424-12.854,5.424s-9.227-1.807-12.847-5.424c-3.614-3.617-5.421-7.898-5.421-12.844 c0-4.948,1.807-9.236,5.421-12.847c3.62-3.62,7.898-5.431,12.847-5.431s9.232,1.811,12.854,5.431 c3.613,3.61,5.421,7.898,5.421,12.847C365.446,406.942,363.638,411.224,360.025,414.841z M433.109,414.841 c-3.614,3.617-7.898,5.424-12.848,5.424c-4.948,0-9.229-1.807-12.847-5.424c-3.613-3.617-5.42-7.898-5.42-12.844 c0-4.948,1.807-9.236,5.42-12.847c3.617-3.62,7.898-5.431,12.847-5.431c4.949,0,9.233,1.811,12.848,5.431 c3.617,3.61,5.427,7.898,5.427,12.847C438.536,406.942,436.729,411.224,433.109,414.841z"></path>
                                    <path
                                        d="M224.692,323.479c3.428,3.613,7.71,5.421,12.847,5.421c5.141,0,9.418-1.808,12.847-5.421l127.907-127.908 c5.899-5.519,7.234-12.182,3.997-19.986c-3.23-7.421-8.847-11.132-16.844-11.136h-73.091V36.543c0-4.948-1.811-9.231-5.421-12.847 c-3.62-3.617-7.901-5.426-12.847-5.426h-73.096c-4.946,0-9.229,1.809-12.847,5.426c-3.615,3.616-5.424,7.898-5.424,12.847V164.45 h-73.089c-7.998,0-13.61,3.715-16.846,11.136c-3.234,7.801-1.903,14.467,3.999,19.986L224.692,323.479z"></path>
                                </svg>
                            </div>
                        </div>
                        <div class="buttons__right">Скачать</div>
                    </div>
                </a>
            </div>
            <div class="buttons__order buttons__btn_position buttons__order_send d-lg-none"
                 onclick="share('{{ $url }}')">
                <div class="buttons__btn buttons__btn_send">
                    <div class="buttons__left">
                        <div class="buttons__icon buttons__icon_padding">
                            <svg viewBox="0 0 511.626 511.626">
                                <path
                                    d="M49.106,178.729c6.472,4.567,25.981,18.131,58.528,40.685c32.548,22.554,57.482,39.92,74.803,52.099 c1.903,1.335,5.946,4.237,12.131,8.71c6.186,4.476,11.326,8.093,15.416,10.852c4.093,2.758,9.041,5.852,14.849,9.277 c5.806,3.422,11.279,5.996,16.418,7.7c5.14,1.718,9.898,2.569,14.275,2.569h0.287h0.288c4.377,0,9.137-0.852,14.277-2.569 c5.137-1.704,10.615-4.281,16.416-7.7c5.804-3.429,10.752-6.52,14.845-9.277c4.093-2.759,9.229-6.376,15.417-10.852 c6.184-4.477,10.232-7.375,12.135-8.71c17.508-12.179,62.051-43.11,133.615-92.79c13.894-9.703,25.502-21.411,34.827-35.116 c9.332-13.699,13.993-28.07,13.993-43.105c0-12.564-4.523-23.319-13.565-32.264c-9.041-8.947-19.749-13.418-32.117-13.418H45.679 c-14.655,0-25.933,4.948-33.832,14.844C3.949,79.562,0,91.934,0,106.779c0,11.991,5.236,24.985,15.703,38.974 C26.169,159.743,37.307,170.736,49.106,178.729z"></path>
                                <path
                                    d="M483.072,209.275c-62.424,42.251-109.824,75.087-142.177,98.501c-10.849,7.991-19.65,14.229-26.409,18.699 c-6.759,4.473-15.748,9.041-26.98,13.702c-11.228,4.668-21.692,6.995-31.401,6.995h-0.291h-0.287 c-9.707,0-20.177-2.327-31.405-6.995c-11.228-4.661-20.223-9.229-26.98-13.702c-6.755-4.47-15.559-10.708-26.407-18.699 c-25.697-18.842-72.995-51.68-141.896-98.501C17.987,202.047,8.375,193.762,0,184.437v226.685c0,12.57,4.471,23.319,13.418,32.265 c8.945,8.949,19.701,13.422,32.264,13.422h420.266c12.56,0,23.315-4.473,32.261-13.422c8.949-8.949,13.418-19.694,13.418-32.265 V184.437C503.441,193.569,493.927,201.854,483.072,209.275z"></path>
                            </svg>
                        </div>
                    </div>
                    <div class="buttons__right">ОТПРАВИТЬ БЕСПЛАТНО</div>
                </div>
            </div>
            <div class="buttons__order buttons__btn_position buttons__order_send d-none d-lg-block">
                <button class="buttons__btn buttons__btn_send" id="sendSocial" data-toggle="modal"
                        data-target=".bd-example-modal-lg">
                    <div class="buttons__left">
                        <div class="buttons__icon buttons__icon_padding">
                            <svg viewBox="0 0 511.626 511.626">
                                <path
                                    d="M49.106,178.729c6.472,4.567,25.981,18.131,58.528,40.685c32.548,22.554,57.482,39.92,74.803,52.099 c1.903,1.335,5.946,4.237,12.131,8.71c6.186,4.476,11.326,8.093,15.416,10.852c4.093,2.758,9.041,5.852,14.849,9.277 c5.806,3.422,11.279,5.996,16.418,7.7c5.14,1.718,9.898,2.569,14.275,2.569h0.287h0.288c4.377,0,9.137-0.852,14.277-2.569 c5.137-1.704,10.615-4.281,16.416-7.7c5.804-3.429,10.752-6.52,14.845-9.277c4.093-2.759,9.229-6.376,15.417-10.852 c6.184-4.477,10.232-7.375,12.135-8.71c17.508-12.179,62.051-43.11,133.615-92.79c13.894-9.703,25.502-21.411,34.827-35.116 c9.332-13.699,13.993-28.07,13.993-43.105c0-12.564-4.523-23.319-13.565-32.264c-9.041-8.947-19.749-13.418-32.117-13.418H45.679 c-14.655,0-25.933,4.948-33.832,14.844C3.949,79.562,0,91.934,0,106.779c0,11.991,5.236,24.985,15.703,38.974 C26.169,159.743,37.307,170.736,49.106,178.729z"></path>
                                <path
                                    d="M483.072,209.275c-62.424,42.251-109.824,75.087-142.177,98.501c-10.849,7.991-19.65,14.229-26.409,18.699 c-6.759,4.473-15.748,9.041-26.98,13.702c-11.228,4.668-21.692,6.995-31.401,6.995h-0.291h-0.287 c-9.707,0-20.177-2.327-31.405-6.995c-11.228-4.661-20.223-9.229-26.98-13.702c-6.755-4.47-15.559-10.708-26.407-18.699 c-25.697-18.842-72.995-51.68-141.896-98.501C17.987,202.047,8.375,193.762,0,184.437v226.685c0,12.57,4.471,23.319,13.418,32.265 c8.945,8.949,19.701,13.422,32.264,13.422h420.266c12.56,0,23.315-4.473,32.261-13.422c8.949-8.949,13.418-19.694,13.418-32.265 V184.437C503.441,193.569,493.927,201.854,483.072,209.275z"></path>
                            </svg>
                        </div>
                    </div>
                    <div class="buttons__right">ОТПРАВИТЬ БЕСПЛАТНО</div>
                </button>
            </div>
        </div>
    </div>

    <div class="card mb-2 bg-primary top-menu">
        <div class="card-body">
            <h4 class="h4 mb-0 text-center font-weight-bolder text-white">Рекомендуем</h4>
        </div>
    </div>

    <div class="card mb-2">
        <div class="card-body">
            <div class="row">
                @foreach($recommended as $postcard)
                    <div class="col-lg-3 col-6 postcard px-sm-3 px-1">
                        <a href="{{ $postcard->link }}">
                            @if(isset($play) && $play == true)
                                <div class="image lazy" style="background-image:url({{ $postcard->image ?? '' }});"></div>
                            @else
                                <div class="image lazy" style="background-image:url({{ $postcard->thumb_image ?? '' }});"
                                     data-src="{{ $postcard->image ?? '' }}"></div>
                            @endif
                            <p class="text-center text-dark text mt-1 px-3">{{ $postcard->name }}</p>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary shadow">
                    <h5 class="modal-title text-white">Отправить открытку в социальных сетях</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="text-white">&times;</span>
                    </button>
                </div>
                <div class="modal__body py-4">
                    <ol class="steps pr-4">
                        <li class="mb-2">
                            <p class="mb-1">
                                Выделите весь текст в поле ниже. Нажмите на выделенном тексте правой кнопкой мыши.
                                Выберите пункт “Копировать“.
                            </p>
                            <input type="text" value="{{ request()->url() }}" id="copyTarget" class="form-control"
                                   readonly>
                        </li>
                        <li class="mb-2">Переходите в социальную сеть, в которой вы хотите отправить открытку - нажав
                            кнопку ниже:
                            <div>
                                <a href="https://ok.ru/messages" target="_blank" class="btn text-white shadow mt-2 mr-2"
                                   style="background-color: #ed812b;border-color: #df731d;">
                                    <svg class="modal__social-icon modal__social-icon_margin"
                                         xmlns="http://www.w3.org/2000/svg" aria-label="simpleicons-vk-icon"
                                         viewBox="0 0 24 24" id="icon-app-vkontakte">
                                        <title>VK icon</title>
                                        <path xmlns="http://www.w3.org/2000/svg"
                                              d="M14.505 17.44a11.599 11.599 0 0 0 3.6-1.49 1.816 1.816 0 0 0-1.935-3.073 7.866 7.866 0 0 1-8.34 0 1.814 1.814 0 0 0-2.5.565c0 .002 0 .004-.002.005a1.812 1.812 0 0 0 .567 2.5l.002.002c1.105.695 2.322 1.2 3.596 1.488l-3.465 3.465A1.796 1.796 0 0 0 6 23.439l.03.03c.344.354.81.53 1.274.53.465 0 .93-.176 1.275-.53L12 20.065l3.404 3.406a1.815 1.815 0 0 0 2.566-2.565l-3.465-3.466zM12 12.388a6.202 6.202 0 0 0 6.195-6.193C18.195 2.78 15.415 0 12 0S5.805 2.78 5.805 6.197A6.2 6.2 0 0 0 12 12.389zm0-8.757a2.566 2.566 0 0 1 0 5.13 2.569 2.569 0 0 1-2.565-2.564A2.57 2.57 0 0 1 12 3.63z"/>
                                    </svg>
                                    Одноклассники
                                </a>
                                <a href="https://my.mail.ru/?browse=messages" target="_blank"
                                   class="btn text-white shadow mt-2 mr-2"
                                   style="background-color: #168de2;border-color: #1476bc;">
                                    <svg class="modal__social-icon modal__social-icon_margin"
                                         xmlns="http://www.w3.org/2000/svg" aria-label="simpleiconsMailruIcon"
                                         viewBox="0 0 24 24" id="icon-app-mailru">
                                        <title>Mail.Ru icon</title>
                                        <path
                                            d="M11.585 5.267c1.834 0 3.558.811 4.824 2.08v.004c0-.609.41-1.068.979-1.068h.145c.891 0 1.073.842 1.073 1.109l.005 9.475c-.063.621.64.941 1.029.543 1.521-1.564 3.342-8.038-.946-11.79-3.996-3.497-9.357-2.921-12.209-.955-3.031 2.091-4.971 6.718-3.086 11.064 2.054 4.74 7.931 6.152 11.424 4.744 1.769-.715 2.586 1.676.749 2.457-2.776 1.184-10.502 1.064-14.11-5.188C-.977 13.521-.847 6.093 5.62 2.245 10.567-.698 17.09.117 21.022 4.224c4.111 4.294 3.872 12.334-.139 15.461-1.816 1.42-4.516.037-4.498-2.031l-.019-.678c-1.265 1.256-2.948 1.988-4.782 1.988-3.625 0-6.813-3.189-6.813-6.812 0-3.659 3.189-6.885 6.814-6.885zm4.561 6.623c-.137-2.653-2.106-4.249-4.484-4.249h-.09c-2.745 0-4.268 2.159-4.268 4.61 0 2.747 1.842 4.481 4.256 4.481 2.693 0 4.464-1.973 4.592-4.306l-.006-.536z"></path>
                                    </svg>
                                    Мой мир
                                </a>
                                <a href="https://vk.com/im" target="_blank" class="btn text-white shadow mt-2 mr-2"
                                   style="background-color: #45668e;border-color: #395270;">
                                    <svg class="modal__social-icon modal__social-icon_margin"
                                         xmlns="http://www.w3.org/2000/svg" aria-label="simpleicons-vk-icon"
                                         viewBox="0 0 24 24" id="icon-app-vkontakte">
                                        <title>VK icon</title>
                                        <path
                                            d="M11.701 18.771h1.437s.433-.047.654-.284c.21-.221.21-.63.21-.63s-.031-1.927.869-2.21c.887-.281 2.012 1.86 3.211 2.683.916.629 1.605.494 1.605.494l3.211-.044s1.682-.105.887-1.426c-.061-.105-.451-.975-2.371-2.76-2.012-1.861-1.742-1.561.676-4.787 1.469-1.965 2.07-3.166 1.875-3.676-.166-.48-1.26-.361-1.26-.361l-3.602.031s-.27-.031-.465.09c-.195.119-.314.391-.314.391s-.572 1.529-1.336 2.82c-1.623 2.729-2.268 2.879-2.523 2.699-.604-.391-.449-1.58-.449-2.432 0-2.641.404-3.75-.781-4.035-.39-.091-.681-.15-1.685-.166-1.29-.014-2.378.01-2.995.311-.405.203-.72.652-.539.675.24.03.779.146 1.064.537.375.506.359 1.636.359 1.636s.211 3.116-.494 3.503c-.495.262-1.155-.28-2.595-2.756-.735-1.26-1.291-2.67-1.291-2.67s-.105-.256-.299-.406c-.227-.165-.557-.225-.557-.225l-3.435.03s-.51.016-.689.24c-.166.195-.016.615-.016.615s2.686 6.287 5.732 9.453c2.79 2.902 5.956 2.715 5.956 2.715l-.05-.055z"></path>
                                    </svg>
                                    Вконтакте
                                </a>
                                <a href="https://facebook.com/messages/" target="_blank"
                                   class="btn text-white shadow mt-2 mr-2"
                                   style="background-color: #3b5998;border-color: #344b7d;">
                                    <svg class="modal__social-icon modal__social-icon_margin"
                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 96.124 96.123"
                                         id="icon-app-facebook">
                                        <path
                                            d="M72.089.02L59.624 0C45.62 0 36.57 9.285 36.57 23.656v10.907H24.037a1.96 1.96 0 0 0-1.96 1.961v15.803a1.96 1.96 0 0 0 1.96 1.96H36.57v39.876a1.96 1.96 0 0 0 1.96 1.96h16.352a1.96 1.96 0 0 0 1.96-1.96V54.287h14.654a1.96 1.96 0 0 0 1.96-1.96l.006-15.803a1.963 1.963 0 0 0-1.961-1.961H56.842v-9.246c0-4.444 1.059-6.7 6.848-6.7l8.397-.003a1.96 1.96 0 0 0 1.959-1.96V1.98A1.96 1.96 0 0 0 72.089.02z"></path>
                                    </svg>
                                    Фейсбук
                                </a>
                            </div>
                        </li>
                        <li class="mb-2">
                            В раздел - сообщения, выберите друга, которому хотите отправить открытку.
                        </li>
                        <li class="mb-2">
                            Там, где обычно пишете ему сообщение, нажимаете правой клавишей мыши.
                        </li>
                        <li class="mb-2">
                            Выберите пункт “Вставить“, потом “Отправить” (Enter).
                        </li>
                    </ol>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-primary shadow" data-dismiss="modal">Я все понял</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <style>
        .prevNext {
            font-size: 30px;
            text-decoration: none !important;
        }

        h1 {
            text-shadow: 0px 2px 0px #a22724;
        }

        .breadcrumb {
            background: #ffffff;
            border-bottom: 3px solid #d3d3d3;
        }

        .slider .postcard .image {
            height: 235px;
        }
    </style>
@endpush

@push('scripts')
    <script>
        function share(link) {
            try {
                Android.showToast("test message");
            } catch (e) {
                navigator.share({
                    title: document.title,
                    text: document.title,
                    url: link,
                });
            }
        }
    </script>
@endpush
