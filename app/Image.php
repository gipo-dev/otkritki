<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image as Imager;

class Image extends File
{
    public static function upload($image, $addingPath = '/documents') {
        $path = parent::upload($image, $addingPath);
        return $path;
    }

    public static function uploadCustom($image, $file, $addingPath = '', $addingName = '', $extfrom = '', $extto = '') {
        $save_path = 'storage/uploads/'.$addingPath;
        if (!file_exists($save_path)) {
            mkdir($save_path, 666, true);
        }
        if($extfrom != '')
            $name = str_replace($extfrom, $extto, $file->getClientOriginalName());
        else
            $name = $file->getClientOriginalName();
        $img = $image->save($save_path.$addingName.Carbon::now()->timestamp.$name);
        $file = new File();
        $file->user_id = Auth::user()->id;
        $file->path = '/'.$img->dirname.'/'.$img->basename;
        $file->save();
        return $file->id;
    }
}
