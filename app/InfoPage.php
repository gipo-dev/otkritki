<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoPage extends Model
{
    public $timestamps = false;

    protected $fillable = ['title', 'body', 'path', 'meta_title', 'meta_description'];
}
