<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Feast extends ICategory
{
    protected $table = 'categories';

    protected $attributes = ['is_feast' => true];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('feast_only', function (Builder $builder) {
            $builder->where('is_feast', 1);
        });
    }

    public function date() {
        return $this->hasOne(FeastDate::class);
    }
}
class FeastDate extends Model {

    public $timestamps = false;

    protected $fillable = ['feast_id', 'date'];

    public function getDateAttribute() {
        return Carbon::create($this->attributes['date'])->format('Y-m-d');
    }
    public function getShortDateAttribute() {
        return Carbon::create($this->attributes['date'])->format('m-d');
    }

    public function feasts() {
        return $this->belongsTo(Feast::class, 'feast_id');
    }

}
