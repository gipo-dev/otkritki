<?php

namespace App\Http\Controllers;

use App\Category;
use App\CategoryBackground;
use App\Postcard;
use App\PostcardPopularity;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PostcardController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $postcard_id)
    {
        $postcard = Postcard::find($postcard_id);
        $popularity = $postcard->addView()->views;
        $background = null;
        $prevNext = Postcard::inRandomOrder()->limit(2)->get();
        $category = null;
        $recommended = Postcard::where('id', '!=', $postcard_id)->inRandomOrder()->limit(10)->get();
        return view('postcard', [
            'postcard' => $postcard,
            'category' => $category,
            'background' => $background->path ?? '',
            'prev_next' => $prevNext,
            'recommended' => $recommended,
            'popularity' => $popularity,
            'url' => $request->url()
        ]);
    }

    public function showApi(Request $request, $postcard_id)
    {
        $postcard = Postcard::find($postcard_id);
        $popularity = $postcard->addView()->views;
        $background = null;
        $prevNext = Postcard::inRandomOrder()->limit(2)->get();
        $category = null;
        $recommended = Postcard::where('id', '!=', $postcard_id)->inRandomOrder()->limit(10)->get();
        return response()->json([
            'postcard' => $postcard,
            'category' => $category,
            'background' => $background->path ?? '',
            'prev_next' => $prevNext,
            'recommended' => $recommended,
            'popularity' => $popularity,
        ]);
    }

    public function showCategory(Request $request, $category, $postcard)
    {
        $popularity = $postcard->addView()->views;
        if ($category != null) {
            $category = Category::withoutGlobalScope('no_feast')
                ->where('slug', $category)->first();
            if ($category != null)
                $background = CategoryBackground::find($category->background_id);
            else
                $background = null;
            $prevNext = $category->postcards()->where('id', '!=', $postcard->id)->inRandomOrder()->limit(2)->get();
            $recommended = $category->postcards()->where('id', '!=', $postcard->id)->inRandomOrder()->limit(4)->get();
            if($recommended->count() == 0)
                $recommended = Postcard::where('id', '!=', $postcard->id)->inRandomOrder()->limit(4)->get();
        } else {
            $background = null;
            $prevNext = Postcard::inRandomOrder()->limit(2)->get();
            $recommended = Postcard::where('id', '!=', $postcard->id)->inRandomOrder()->limit(4)->get();
        };
        return view('postcard', [
            'postcard' => $postcard,
            'category' => $category,
            'background' => $background->path ?? '',
            'prev_next' => $prevNext,
            'recommended' => $recommended,
            'popularity' => $popularity,
            'url' => $request->url(),
        ]);
    }

    public function showCategoryApi(Request $request, $category, $postcard)
    {
        $popularity = $postcard->addView()->views;
        if ($category != null) {
            $category = Category::withoutGlobalScope('no_feast')
                ->where('slug', $category)->first();
            if ($category != null)
                $background = CategoryBackground::find($category->background_id);
            else
                $background = null;
            $prevNext = $category->postcards()->where('id', '!=', $postcard->id)->inRandomOrder()->limit(2)->get();
            $recommended = $category->postcards()->where('id', '!=', $postcard->id)->inRandomOrder()->limit(4)->get();
            if($recommended->count() == 0)
                $recommended = Postcard::where('id', '!=', $postcard->id)->inRandomOrder()->limit(4)->get();
        } else {
            $background = null;
            $prevNext = Postcard::inRandomOrder()->limit(2)->get();
            $recommended = Postcard::where('id', '!=', $postcard->id)->inRandomOrder()->limit(4)->get();
        };
        return response()->json([
            'postcard' => $postcard,
            'category' => $category,
            'background' => $background->path ?? '',
            'prev_next' => $prevNext,
            'recommended' => $recommended,
            'popularity' => $popularity,
            'url' => $request->url(),
        ]);
    }
}
