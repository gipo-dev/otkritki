<?php

namespace App\Http\Controllers;

use App\Category;
use App\Feast;
use App\FeastDate;
use App\InfoPage;
use App\Postcard;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Jenssegers\Agent\Facades\Agent;
use Spatie\Sitemap\SitemapGenerator;

class WebsiteController extends Controller
{
    public function index()
    {
        $postcards = Postcard::has('categories', '>', 0)->inRandomOrder()->limit(30)->get();
        $categories = Category::has('childrens', 0)
            ->with('parent')
            ->inRandomOrder()
            ->limit(10)->with('randomItem')->get();
        return view('index', [
            'postcards' => $postcards,
            'categories' => $categories,
        ]);
    }

    public function indexApi()
    {
        $postcards = Postcard::inRandomOrder()->limit(30)->get();
        $categories = Category::whereNotNull('parent_id')->inRandomOrder()->limit(10)->with('randomItem')->get();
        return response()->json([
            'postcards' => $postcards,
            'categories' => $categories,
        ]);
    }

    public static function getFeasts($count = 7)
    {
        $feasts = FeastDate::with('feasts');
        $days = [];
        for ($i = 0; $i < $count; $i++) {
            $feasts->orWhere(function ($query) use ($i, &$days) {
                $now = Carbon::now();
                $query->whereDay('date', $now->addDay($i)->day);
                $query->whereMonth('date', $now->month);
                $d = new FeastDate();
                $d->day = $now;
                $days[] = $d;
            });
        }
        $feasts = $feasts->get()->groupBy('short_date');
        foreach ($days as $day) {
            $format = $day->day->format('m-d');
            if (isset($feasts[$format])) {
                $day->feasts = $feasts[$format];
            }
        }
        return $days;
    }

    public function generateSitemap(Request $request) {
        SitemapGenerator::create('http://127.0.0.1:8000')
            ->writeToFile(public_path('sitemap.xml'));
    }

    public function info(Request $request, $page_name) {
        $info_page = InfoPage::where('path', $page_name)->firstOrFail();
        return view('info_page', ['page' => $info_page]);
    }
    public function infoApi(Request $request, $page_name) {
        $info_page = InfoPage::where('path', $page_name)->firstOrFail();
        return response()->json($info_page);
    }
}
