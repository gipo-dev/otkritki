<?php

namespace App\Http\Controllers;

use App\Category;
use App\Feast;
use App\FeastDate;
use App\Postcard;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class CategoryController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $categorySlug, $postcardOrCategory = null, $postcard = null)
    {
        if ($postcard != null || $postcardOrCategory != null) {
            $slug = $postcardOrCategory;
            $postcard = Postcard::where('slug', $postcardOrCategory)
                ->orderBy('created_at', 'desc')
                ->orWhere('slug', $postcard)->first();
            if ($postcard != null) {
                $categorySlug = $postcard->slug == $postcardOrCategory ? $categorySlug : $postcardOrCategory;
                $_pc = new PostcardController();
                $request->isPostcard = true;
                return $_pc->showCategory($request, $categorySlug, $postcard);
            }
        } else
            $slug = $categorySlug;

        $category = Category::withoutGlobalScope('no_feast')
            ->where('slug', $slug)->with('parent', 'parent.childs', 'childs')->first();
        if ($category == null)
            abort(404);
        $postcards = $category->postcards()
            ->orderBy('created_at', 'desc')->paginate(18);
        if($postcards->count() == 0 && $category->childs->count() > 0) {
            $subCategories = $category->childs->pluck('id')->toArray();
            $postcards = $category->postcards()
                ->orWhereIn('category_id', $subCategories)
                ->groupBy('postcard_id')
                ->orderBy('created_at', 'desc')
                ->paginate(18);
        }

        if($request->api == 1) {
            return view('partials.postcards_block', ['postcards' => $postcards]);
        }

        return view('category', [
            'category' => $category,
            'postcards' => $postcards
        ]);
    }

    public function showApi(Request $request, $categorySlug, $postcardOrCategory = null, $postcard = null)
    {
        if ($postcard != null || $postcardOrCategory != null) {
            $slug = $postcardOrCategory;
            $postcard = Postcard::where('slug', $postcardOrCategory)
                ->orderBy('created_at', 'desc')
                ->orWhere('slug', $postcard)->first();
            if ($postcard != null) {
                $categorySlug = $postcard->slug == $postcardOrCategory ? $categorySlug : $postcardOrCategory;
                $_pc = new PostcardController();
                $request->isPostcard = true;
                return $_pc->showCategoryApi($request, $categorySlug, $postcard);
            }
        } else
            $slug = $categorySlug;

        $category = Category::withoutGlobalScope('no_feast')
            ->where('slug', $slug)->with('parent', 'parent.childs', 'childs')->first();
        if ($category == null)
            abort(404);
        $postcards = $category->postcards()
            ->orderBy('created_at', 'desc')->paginate(18);
        if($postcards->count() == 0 && $category->childs->count() > 0) {
            $subCategories = $category->childs->pluck('id')->toArray();
            $postcards = $category->postcards()
                ->orWhereIn('category_id', $subCategories)
                ->groupBy('postcard_id')
                ->orderBy('created_at', 'desc')
                ->paginate(18);
        }

        return response()->json([
            'category' => $category,
            'postcards' => $postcards
        ]);
    }

    public function feasts(Request $request)
    {
        $days = collect(WebsiteController::getFeasts(365))
            ->groupBy(function ($d) {
                return Carbon::parse($d->day)->format('my');
            });
        $months = [];
        for ($m = 0; $m < 24; $m++) {
            $mn = Carbon::now()->day(1)->addMonth($m);
            $months[$mn->format('my')] = $mn->monthName;
        }

        $mlist = [];
        for ($i = Carbon::now()->format('m'); $i < Carbon::now()->format('m') + 12; $i++) {
            $m = Carbon::now()->month($i);
            $mlist[] = [
                'name' => $m->monthName,
                'slug' => strtolower($m->format('F')),
            ];
        }
        return view('feasts', [
            'day_group' => $days,
            'months' => $months,
            'mlist' => $mlist,
        ]);
    }

    public function feast(Request $request, $month_name)
    {
        $feasts = FeastDate::with('feasts');
        $days = [];
        $month_id = Carbon::parse($month_name)->month;
        for ($i = 0; $i < 31; $i++) {
            $feasts->orWhere(function ($query) use ($i, &$days, $month_id) {
                $now = \Carbon\Carbon::now()->day(1)->month($month_id);
                $query->whereDay('date', $now->addDay($i)->day);
                $query->whereMonth('date', $now->month);
                $d = new FeastDate();
                $d->day = $now;
                $days[] = $d;
            });
        }
        $feasts = $feasts->get()->groupBy('short_date');
        foreach ($days as $day) {
            $format = $day->day->format('m-d');
            if (isset($feasts[$format])) {
                $day->feasts = $feasts[$format];
            }
        }

        foreach ($days as $k => $d)
            if ($d->day->month != $month_id)
                unset($days[$k]);

        $days = collect($days)
            ->groupBy(function ($d) {
                return Carbon::parse($d->day)->format('my');
            });
        $months = [];
        for ($m = 1; $m < 24; $m++) {
            $mn = Carbon::now()->month($m);
            $months[$mn->format('my')] = $mn->monthName;
        }

        $mlist = [];
        for ($i = Carbon::now()->format('m'); $i < Carbon::now()->format('m') + 12; $i++) {
            $m = Carbon::now()->month($i);
            $mlist[] = [
                'name' => $m->monthName,
                'slug' => strtolower($m->format('F')),
            ];
        }
        return view('feasts', [
            'day_group' => $days,
            'months' => $months,
            'mlist' => $mlist,
        ]);
    }

    public function feastApi(Request $request, $month_name)
    {
        $feasts = FeastDate::with('feasts');
        $days = [];
        $month_id = Carbon::parse($month_name)->month;
        for ($i = 0; $i < 31; $i++) {
            $feasts->orWhere(function ($query) use ($i, &$days, $month_id) {
                $now = \Carbon\Carbon::now()->day(1)->month($month_id);
                $query->whereDay('date', $now->addDay($i)->day);
                $query->whereMonth('date', $now->month);
                $d = new FeastDate();
                $d->day = $now;
                $days[] = $d;
            });
        }
        $feasts = $feasts->get()->groupBy('short_date');
        foreach ($days as $day) {
            $format = $day->day->format('m-d');
            if (isset($feasts[$format])) {
                $day->feasts = $feasts[$format];
            }
        }

        foreach ($days as $k => $d)
            if ($d->day->month != $month_id)
                unset($days[$k]);

        $days = collect($days)
            ->groupBy(function ($d) {
                return Carbon::parse($d->day)->format('my');
            });
        $months = [];
        for ($m = 1; $m < 24; $m++) {
            $mn = Carbon::now()->day(1)->addMonth($m);
            $months[$mn->format('my')] = $mn->monthName;
        }

        $mlist = [];
        for ($i = Carbon::now()->format('m'); $i < Carbon::now()->format('m') + 12; $i++) {
            $m = Carbon::now()->month($i);
            $mlist[] = [
                'name' => $m->monthName,
                'slug' => strtolower($m->format('F')),
            ];
        }
        return response()->json('feasts', [
            'day_group' => $days,
            'months' => $months,
            'mlist' => $mlist,
        ]);
    }

    public function feastsApi(Request $request)
    {
        $days = collect(WebsiteController::getFeasts(365))
            ->groupBy(function ($d) {
                return Carbon::parse($d->day)->format('my');
            });
        $months = [];
        for ($m = 1; $m < 24; $m++) {
            $mn = Carbon::now()->month($m);
            $months[$mn->format('my')] = $mn->monthName;
        }

        $mlist = [];
        for ($i = Carbon::now()->format('m'); $i < Carbon::now()->format('m') + 12; $i++) {
            $m = Carbon::now()->month($i);
            $mlist[] = [
                'name' => $m->monthName,
                'slug' => strtolower($m->format('F')),
            ];
        }
        return response()->json([
            'day_group' => $days,
            'months' => $months,
            'mlist' => $mlist,
        ]);
    }

    public function catalog(Request $request)
    {
        $categories = Category::all();
        return view('catalog', [
            'categories' => $categories,
        ]);
    }

    public function catalogApi(Request $request)
    {
        $categories = Category::all();
        return response()->json([
            'categories' => $categories,
        ]);
    }
}
