<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\CategoryBackground;
use App\CategoryBackgroundCategory;
use App\File;
use App\Image as ImageAlias;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image as Imager;
use IvanLemeshev\Laravel5CyrillicSlug\Slug;
use IvanLemeshev\Laravel5CyrillicSlug\SlugFacade;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::with('background', 'parent')->paginate(20);
        $rows = [];
        foreach ($categories as $k => $item) {
            $rows[] = [
                'values' => [
                    $item->image ?? '',
                    $item->name ?? '',
                    $item->parent ? $item->parent->name : 'Нет',
                ],
                'link' => route('admin.categories.edit', $item->id),
            ];
        }
        $table = [
            'columns' => ['Изображение', 'Имя', 'Родительская категория'],
            'rows' => $rows,
        ];
        return view('admin.layouts.table_image_page', [
            'table' => $table,
            'title' => 'категорий',
            'create' => route('admin.categories.create'),
            'pagination' => $categories,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form = [
            [
                'type' => 'text',
                'name' => 'name',
                'text' => 'Название',
                'required' => true,
                'value' => ''
            ],
            [
                'type' => 'select',
                'name' => 'parent_id',
                'text' => 'Родительская категория',
                'required' => false,
                'value' => '',
                'items' => Category::whereNull('parent_id')->get()->keyBy('id')
            ],
            [
                'type' => 'select',
                'name' => 'background_id',
                'text' => 'Фон категории',
                'required' => false,
                'value' => '',
                'items' => CategoryBackground::with('category')->get()->keyBy('id')->sortBy('background_id'),
            ],
            [
                'type' => 'image',
                'name' => 'icon',
                'text' => 'Иконка',
                'required' => false,
            ],
            [
                'type' => 'color',
                'name' => 'color',
                'text' => 'Цвет фона',
                'required' => true,
            ],
            [
                'type' => 'image',
                'name' => 'image',
                'text' => 'Изображение',
                'required' => true,
            ],
            [
                'type' => 'text-editable',
                'name' => 'description',
                'text' => 'Описание',
                'required' => false,
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'meta_title',
                'text' => 'Мета заголовок',
                'required' => true,
                'value' => ''
            ],
            [
                'type' => 'textarea',
                'name' => 'meta_description',
                'text' => 'Мета описание',
                'required' => true,
                'value' => ''
            ],
            [
                'type' => 'text',
                'name' => 'meta_keywords',
                'text' => 'Мета ключевые слова',
                'required' => false,
                'value' => ''
            ],
        ];
        return view('admin.layouts.edit_form', [
            'form' => $form,
            'method' => 'post',
            'form_route' => route('admin.categories.store'),
            'title' => 'категории'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->remove('_token');
        $request->request->remove('_method');
        if (isset($request->icon)) {
            $request->request->set('icon', File::find(
                ImageAlias::uploadCustom(Imager::make($request->icon)
                    ->resize(100, null, function ($constraint) {
                        $constraint->aspectRatio();
                    }), $request->icon, 'icons/')
            )->path);
        }
        if (isset($request->image)) {
            $request->request->set('image', File::find(
                ImageAlias::uploadCustom(Imager::make($request->image)
                    ->resize(null, 195, function ($constraint) {
                        $constraint->aspectRatio();
                    }), $request->image, 'category-images/')
            )->path);
        }
        $request->request->set('sort', 0);
        $request->request->set('slug', SlugFacade::make($request->name));
        Category::create($request->request->all());
        return redirect(route('admin.categories.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        $form = [
            [
                'type' => 'text',
                'name' => 'name',
                'text' => 'Название',
                'required' => true,
                'value' => $category->name,
            ],
            [
                'type' => 'select',
                'name' => 'parent_id',
                'text' => 'Родительская категория',
                'required' => false,
                'value' => $category->parent_id,
                'items' => Category::whereNull('parent_id')->get()->keyBy('id'),
            ],
            [
                'type' => 'select',
                'name' => 'background_id',
                'text' => 'Фон категории',
                'required' => false,
                'value' => $category->background_id,
                'items' => CategoryBackground::with('category')->get()->keyBy('id')->sortBy('background_id'),
            ],
            [
                'type' => 'image',
                'name' => 'icon',
                'text' => 'Иконка',
                'required' => false,
                'value' => $category->icon,
            ],
            [
                'type' => 'color',
                'name' => 'color',
                'text' => 'Цвет фона',
                'required' => true,
                'value' => $category->color,
            ],
            [
                'type' => 'image',
                'name' => 'image',
                'text' => 'Изображение',
                'required' => false,
                'value' => $category->image,
            ],
            [
                'type' => 'text-editable',
                'name' => 'description',
                'text' => 'Описание',
                'required' => false,
                'value' => $category->description,
            ],
            [
                'type' => 'text',
                'name' => 'meta_title',
                'text' => 'Мета заголовок',
                'required' => true,
                'value' => $category->meta_title
            ],
            [
                'type' => 'textarea',
                'name' => 'meta_description',
                'text' => 'Мета описание',
                'required' => true,
                'value' => $category->meta_description,
            ],
            [
                'type' => 'text',
                'name' => 'meta_keywords',
                'text' => 'Мета ключевые слова',
                'required' => false,
                'value' => $category->meta_keywords,
            ],
        ];
        return view('admin.layouts.edit_form', [
            'title' => 'категории',
            'form' => $form,
            'method' => 'put',
            'form_route' => route('admin.categories.update', $id),
            'delete' => route('admin.categories.destroy', $id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->request->remove('_token');
        $request->request->remove('_method');
        if (isset($request->icon)) {
            $request->request->set('icon', File::find(
                ImageAlias::uploadCustom(Imager::make($request->icon)
                    ->resize(100, null, function ($constraint) {
                        $constraint->aspectRatio();
                    }), $request->icon, 'icons/')
            )->path);
        }
        if (isset($request->image)) {
            $request->request->set('image', File::find(
                ImageAlias::uploadCustom(Imager::make($request->image)
                    ->resize(null, 195, function ($constraint) {
                        $constraint->aspectRatio();
                    }), $request->image, 'category-images/')
            )->path);
        }
        Category::where('id', $id)
            ->update($request->request->all());
        return redirect(route('admin.categories.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::find($id)->delete();
        return redirect(route('admin.categories.index'));
    }
}

class CategoryBackgroundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $backgrounds = CategoryBackground::with('category')->paginate(20);
        $rows = [];
        foreach ($backgrounds as $k => $item) {
            $rows[] = [
                'values' => [
                    $item->path, $item->name, $item->category ? $item->category->name : 'Не задана',
                ],
                'link' => route('admin.category-backgrounds.edit', $item->id),
            ];
        }
        $table = [
            'columns' => ['Изображение', 'Имя', 'Категория'],
            'rows' => $rows,
        ];
        return view('admin.layouts.table_image_page', [
            'table' => $table,
            'title' => 'фонов',
            'create' => route('admin.category-backgrounds.create'),
            'pagination' => $backgrounds,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form = [
            [
                'type' => 'text',
                'name' => 'name',
                'text' => 'Название категории',
                'required' => true,
                'value' => ''
            ],
            [
                'type' => 'select',
                'name' => 'category_id',
                'text' => 'Категория',
                'required' => true,
                'value' => '',
                'items' => CategoryBackgroundCategory::all()->keyBy('id')
            ],
            [
                'type' => 'image',
                'name' => 'path',
                'text' => 'Изображение',
                'required' => true,
            ],
        ];
        return view('admin.layouts.edit_form', [
            'form' => $form,
            'method' => 'post',
            'form_route' => route('admin.category-backgrounds.store'),
            'title' => 'фона'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->remove('_token');
        $request->request->remove('_method');
        foreach ($request->files as $k => $file) {
            $request->request->set($k, File::find(ImageAlias::upload($file, '/backgrounds'))->path);
        }
        CategoryBackground::create($request->request->all());
        return redirect(route('admin.category-backgrounds.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoryBackground = CategoryBackground::find($id);
        $form = [
            [
                'type' => 'text',
                'name' => 'name',
                'text' => 'Название категории',
                'required' => true,
                'value' => $categoryBackground->name
            ],
            [
                'type' => 'select',
                'name' => 'category_id',
                'text' => 'Категория',
                'required' => true,
                'value' => $categoryBackground->category_id,
                'items' => CategoryBackgroundCategory::all()->keyBy('id')
            ],
            [
                'type' => 'image',
                'name' => 'path',
                'text' => 'Изображение',
                'required' => true,
                'value' => $categoryBackground->path,
            ],
        ];
        return view('admin.layouts.edit_form', [
            'title' => 'фона',
            'form' => $form,
            'method' => 'put',
            'form_route' => route('admin.category-backgrounds.update', $id),
            'delete' => route('admin.category-backgrounds.destroy', $id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->request->remove('_token');
        $request->request->remove('_method');
        foreach ($request->files as $k => $file) {
            $request->request->set($k, File::find(ImageAlias::upload($file, '/backgrounds'))->path);
        }
        CategoryBackground::where('id', $id)
            ->update($request->request->all());
        return redirect(route('admin.category-backgrounds.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CategoryBackground::find($id)->delete();
        return redirect(route('admin.category-backgrounds.index'));
    }
}

class CategoryBackgroundCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = CategoryBackgroundCategory::paginate(20);
        $rows = [];
        foreach ($categories as $k => $item) {
            $rows[] = [
                'values' => [
                    $item->name
                ],
                'link' => route('admin.category-backgrounds-categories.edit', $item->id),
            ];
        }
        $table = [
            'columns' => ['Имя категории'],
            'rows' => $rows,
        ];
        return view('admin.layouts.table_page', [
            'table' => $table,
            'title' => 'категорий фонов',
            'create' => route('admin.category-backgrounds-categories.create'),
            'pagination' => $categories,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form = [
            [
                'type' => 'text',
                'name' => 'name',
                'text' => 'Название категории',
                'required' => true,
                'value' => ''
            ],
        ];
        return view('admin.layouts.edit_form', [
            'form' => $form,
            'method' => 'post',
            'form_route' => route('admin.category-backgrounds-categories.store'),
            'title' => 'категории'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        CategoryBackgroundCategory::create($request->except(['_token', '_method']));
        return redirect(route('admin.category-backgrounds-categories.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoryBackgroundCategory = CategoryBackgroundCategory::findOrFail($id);
        $form = [
            [
                'type' => 'text',
                'name' => 'name',
                'text' => 'Название категории',
                'required' => true,
                'value' => $categoryBackgroundCategory->name,
            ],
        ];
        return view('admin.layouts.edit_form', [
            'title' => 'категории фона ' . $categoryBackgroundCategory->name,
            'form' => $form,
            'method' => 'put',
            'form_route' => route('admin.category-backgrounds-categories.update', $id),
            'delete' => route('admin.category-backgrounds-categories.destroy', $id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        CategoryBackgroundCategory::where('id', $id)
            ->update($request->except(['_token', '_method']));
        return redirect(route('admin.category-backgrounds-categories.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CategoryBackgroundCategory::find($id)->delete();
        return redirect(route('admin.category-backgrounds-categories.index'));
    }
}
