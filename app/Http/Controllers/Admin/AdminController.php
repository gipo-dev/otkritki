<?php

namespace App\Http\Controllers\Admin;

use App\Postcard;
use App\PostcardPopularity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index() {
        $postcards = PostcardPopularity::with(['postcard', 'postcard.categories'])->orderBy('views', 'desc')->paginate(20);
        $rows = [];
        foreach ($postcards as $k => $item) {
            $rows[] = [
                'values' => [
                    $item->postcard->thumb_image ?? '',
                    $item->postcard->name ?? '',
                    $item->postcard->categoryList() ?? '',
                    $item->views ?? 'Просмотров нет',
                ],
                'link' => route('admin.postcards.edit', $item->postcard->id),
            ];
        }
        $table = [
            'columns' => ['Изображение', 'Наименование', 'Категории', 'Кол-во просмотров'],
            'rows' => $rows,
        ];
        return view('admin.layouts.table_image_page', [
            'table' => $table,
            'title' => 'популярных сегодня открыток',
            'pagination' => $postcards,
        ]);
    }

    public static function getMenu() {
        $menu = [
            [
                'name' => 'Главная',
                'url' => route('admin.index')
            ],
            [
                'name' => 'Категории',
                'url' => route('admin.categories.index'),
                'sub' => [
                    [
                        'name' => 'Категории',
                        'url' => route('admin.categories.index')
                    ],
                    [
                        'name' => 'Праздники',
                        'url' => route('admin.feasts.index')
                    ],
                    [
                        'name' => 'Фоны',
                        'url' => route('admin.category-backgrounds.index')
                    ],
                    [
                        'name' => 'Категории фонов',
                        'url' => route('admin.category-backgrounds-categories.index')
                    ],
                ]
            ],
            [
                'name' => 'Открытки',
                'url' => route('admin.postcards.index'),
                'sub' => [
                    [
                        'name' => 'Открытки',
                        'url' => route('admin.postcards.index')
                    ],
                    [
                        'name' => 'Парсер открыток',
                        'url' => route('admin.parser.index')
                    ],
                ]
            ],
        ];
        if(Auth::user()->isSuper) {
            $menu = array_merge($menu, [
                [
                    'name' => 'Пользователи',
                    'url' => route('admin.users.index'),
                ],
                [
                    'name' => 'Инфо-страницы',
                    'url' => route('admin.info-pages.index'),
                ],
            ]);
        }
        return $menu;
    }
}
