<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Image;
use App\Postcard;
use Carbon\Carbon;
use DOMDocument;
use GuzzleHttp\Client;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
//use Intervention\Image\Facades\Image;
use Intervention\Image\Facades\Image as Imager;
use IvanLemeshev\Laravel5CyrillicSlug\SlugFacade;

\Debugbar::disable();

class ParserController extends Controller
{
    public function index()
    {
        $categories = $this->categories();
        return view('admin.parser', [
            'categories' => $categories,
        ]);
    }

    public function make(Request $request)
    {
        $htmlString = file_get_contents($request->website_url);

        $url = parse_url($request->website_url)['scheme'] . '://' . parse_url($request->website_url)['host'];

        $htmlDom = new DOMDocument;

        @$htmlDom->loadHTML($htmlString);

        $imageTags = $htmlDom->getElementsByTagName('img');

        $extractedImages = array();

        foreach ($imageTags as $imageTag) {

            $imgSrc = $imageTag->getAttribute('src');
            if (!isset(parse_url($imgSrc)['scheme'])) {
                $p = $url;
                $imgSrc = $p . $imgSrc;
            }

            $altText = $imageTag->getAttribute('alt');

            $titleText = $imageTag->getAttribute('title');

            $extractedImages[] = array(
                'src' => $imgSrc,
                'alt' => $altText,
                'title' => $titleText,
                'id' => -1,
            );
        }
        return response()->json($extractedImages);
    }

    public function categories()
    {
        $categories = Category::withoutGlobalScope('no_feast')
            ->has('childrens', 0)
            ->orWhere('is_feast', 1)
            ->with('parent')->get()->keyBy('id');
        return $categories;
    }

    public function createPostcard(Request $request)
    {
        $url = $request->path;
        $name = explode('?', substr($url, strrpos($url, '/') + 1))[0];
        $now = Carbon::now()->timestamp;

        $contents = file_get_contents($url);
        $img = Storage::put('public/uploads/postcards/' . $now . $name, $contents);
        $img_path = "/storage/uploads/postcards/" . $now . $name;
        Imager::make($request->path)
            ->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })
            ->encode('jpg', 80)
            ->save('storage/uploads/postcards/thumb_' . $now . str_replace(".gif", ".jpg", $name));

        $file = new \App\File();
        $file->user_id = Auth::user()->id;
        $file->path = $img_path;
        $file->save();

        $postcard = Postcard::create([
            'name' => $request->name,
            'slug' => SlugFacade::make($request->name),
            'path' => $file->path,
        ]);
        $postcard->categories()->attach($request->categories);
        $postcard->slug = $postcard->id.'-'.$postcard->slug;
        $postcard->save();
        return response()->json([
            'id' => $postcard->id,
        ]);
    }

    public function deletePostcard(Request $request)
    {
        Postcard::findOrFail($request->postcard_id)->delete();
    }
}
