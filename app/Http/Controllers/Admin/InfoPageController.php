<?php

namespace App\Http\Controllers\Admin;

use App\InfoPage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InfoPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $info_pages = InfoPage::paginate(20);
        $rows = [];
        foreach ($info_pages as $k => $item) {
            $rows[] = [
                'values' => [
                    $item->title ?? '',
                    str_limit($item->body, 50),
                    $item->path,
                ],
                'link' => route('admin.info-pages.edit', $item->id),
            ];
        }
        $table = [
            'columns' => ['Название', 'Текст', 'Адрес'],
            'rows' => $rows,
        ];
        return view('admin.layouts.table_page', [
            'table' => $table,
            'title' => 'информационных страниц',
            'create' => route('admin.info-pages.create'),
            'pagination' => $info_pages,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form = [
            [
                'type' => 'text',
                'name' => 'title',
                'text' => 'Заголовок',
                'required' => true,
                'value' => '',
            ],
            [
                'type' => 'text-editable',
                'name' => 'body',
                'text' => 'Текст',
                'required' => true,
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'path',
                'text' => 'Адрес страницы',
                'required' => true,
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'meta_title',
                'text' => 'Мета заголовок',
                'required' => true,
                'value' => ''
            ],
            [
                'type' => 'textarea',
                'name' => 'meta_description',
                'text' => 'Мета описание',
                'required' => true,
                'value' => ''
            ],
        ];
        return view('admin.layouts.edit_form', [
            'form' => $form,
            'method' => 'post',
            'form_route' => route('admin.info-pages.store'),
            'title' => 'информационной страницы'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->remove('_token');
        $request->request->remove('_method');
        $info_page = InfoPage::create($request->request->all());
        return redirect(route('admin.info-pages.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $info_page = InfoPage::find($id);
        $form = [
            [
                'type' => 'text',
                'name' => 'title',
                'text' => 'Заголовок',
                'required' => true,
                'value' => $info_page->title,
            ],
            [
                'type' => 'text-editable',
                'name' => 'body',
                'text' => 'Текст',
                'required' => true,
                'value' => $info_page->body,
            ],
            [
                'type' => 'text',
                'name' => 'path',
                'text' => 'Адрес страницы',
                'required' => true,
                'value' => $info_page->path,
            ],
            [
                'type' => 'text',
                'name' => 'meta_title',
                'text' => 'Мета заголовок',
                'required' => true,
                'value' => $info_page->meta_title,
            ],
            [
                'type' => 'textarea',
                'name' => 'meta_description',
                'text' => 'Мета описание',
                'required' => true,
                'value' => $info_page->meta_description,
            ],
        ];
        return view('admin.layouts.edit_form', [
            'form' => $form,
            'method' => 'put',
            'form_route' => route('admin.info-pages.update', $id),
            'delete' => route('admin.info-pages.destroy', $id),
            'title' => 'информационной страницы'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd($request);
        $request->request->remove('_token');
        $request->request->remove('_method');
        $info_page = InfoPage::find($id);
        $info_page->update($request->request->all());
        return redirect(route('admin.info-pages.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        InfoPage::find($id)->delete();
        return redirect(route('admin.info-pages.index'));
    }
}
