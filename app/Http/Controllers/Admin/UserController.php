<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\UserRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(20);
        $rows = [];
        foreach ($users as $k => $item) {
            $rows[] = [
                'values' => [
                    $item->name,
                    $item->email,
                    $item->role_name,
                ],
                'link' => route('admin.users.edit', $item->id),
            ];
        }
        $table = [
            'columns' => ['Имя', 'Почта', 'Роль'],
            'rows' => $rows,
        ];
        return view('admin.layouts.table_page', [
            'table' => $table,
            'title' => 'пользователей',
            'create' => route('admin.users.create'),
            'pagination' => $users,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form = [
            [
                'type' => 'text',
                'name' => 'name',
                'text' => 'Имя',
                'required' => true,
                'value' => ''
            ],
            [
                'type' => 'text',
                'name' => 'email',
                'text' => 'Почта (используется для входа)',
                'required' => true,
                'value' => ''
            ],
            [
                'type' => 'text',
                'name' => 'password',
                'text' => 'Пароль',
                'required' => true,
                'value' => ''
            ],
            [
                'type' => 'select',
                'name' => 'role_id',
                'text' => 'Роль',
                'required' => true,
                'value' => '',
                'items' => UserRole::all()->keyBy('id'),
            ],
        ];
        return view('admin.layouts.edit_form', [
            'form' => $form,
            'method' => 'post',
            'form_route' => route('admin.users.store'),
            'title' => 'пользователя'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->remove('_token');
        $request->request->remove('_method');
        $request->request->set('password', bcrypt($request->password));
        $user = User::create($request->request->all());
        return redirect(route('admin.users.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user = User::findOrFail($id);
        $form = [
            [
                'type' => 'text',
                'name' => 'name',
                'text' => 'Имя',
                'required' => true,
                'value' => $user->name
            ],
            [
                'type' => 'text',
                'name' => 'email',
                'text' => 'Почта (используется для входа)',
                'required' => true,
                'value' => $user->email
            ],
            [
                'type' => 'text',
                'name' => 'password',
                'text' => 'Пароль',
                'required' => false,
                'value' => ''
            ],
            [
                'type' => 'select',
                'name' => 'role_id',
                'text' => 'Роль',
                'required' => true,
                'value' => $user->role_id,
                'items' => UserRole::all()->keyBy('id'),
            ],
        ];
        return view('admin.layouts.edit_form', [
            'form' => $form,
            'method' => 'put',
            'title' => 'пользователя',
            'form_route' => route('admin.users.update', $id),
            'delete' => route('admin.users.destroy', $id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->request->remove('_token');
        $request->request->remove('_method');
        if($request->password != null)
            $request->request->set('password', bcrypt($request->password));
        else
            $request->request->remove('password');
        $user = User::find($id)->update($request->request->all());
        return redirect(route('admin.users.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect(route('admin.users.index'));
    }
}
