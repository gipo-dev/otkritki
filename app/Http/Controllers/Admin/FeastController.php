<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Feast;
use App\FeastDate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use IvanLemeshev\Laravel5CyrillicSlug\SlugFacade;

class FeastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feasts = Feast::with('date')->paginate(20);
        $rows = [];
        foreach ($feasts as $k => $item) {
            $rows[] = [
                'values' => [
                    $item->date->date ?? 'Не установлена',
                    $item->name,
                ],
                'link' => route('admin.feasts.edit', $item->id),
            ];
        }
        $table = [
            'columns' => ['Дата проведения', 'Имя'],
            'rows' => $rows,
        ];
        return view('admin.layouts.table_page', [
            'table' => $table,
            'title' => 'праздников',
            'create' => route('admin.feasts.create'),
            'pagination' => $feasts,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form = [
            [
                'type' => 'text',
                'name' => 'name',
                'text' => 'Название',
                'required' => true,
                'value' => '',
            ],
            [
                'type' => 'date',
                'name' => 'date',
                'text' => 'Дата проведение',
                'required' => true,
                'value' => '',
            ],
            [
                'type' => 'text-editable',
                'name' => 'description',
                'text' => 'Описание',
                'required' => false,
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'meta_title',
                'text' => 'Мета заголовок',
                'required' => true,
                'value' => ''
            ],
            [
                'type' => 'textarea',
                'name' => 'meta_description',
                'text' => 'Мета описание',
                'required' => true,
                'value' => ''
            ],
            [
                'type' => 'text',
                'name' => 'meta_keywords',
                'text' => 'Мета ключевые слова',
                'required' => false,
                'value' => ''
            ],
        ];
        return view('admin.layouts.edit_form', [
            'form' => $form,
            'method' => 'post',
            'form_route' => route('admin.feasts.store'),
            'title' => 'праздника'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->remove('_token');
        $request->request->remove('_method');
        $request->request->set('sort', 0);
        $request->request->set('slug', SlugFacade::make($request->name));
        $feast = Feast::create($request->request->all());
        $feast->date()->create([
            'date' => $request->date,
        ]);
        return redirect(route('admin.feasts.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Feast $feast)
    {
        $form = [
            [
                'type' => 'text',
                'name' => 'name',
                'text' => 'Название',
                'required' => true,
                'value' => $feast->name,
            ],
            [
                'type' => 'date',
                'name' => 'date',
                'text' => 'Дата проведение',
                'required' => true,
                'value' => $feast->date->date ?? '',
            ],
            [
                'type' => 'text-editable',
                'name' => 'description',
                'text' => 'Описание',
                'required' => false,
                'value' => $feast->description,
            ],
            [
                'type' => 'text',
                'name' => 'meta_title',
                'text' => 'Мета заголовок',
                'required' => true,
                'value' => $feast->meta_title
            ],
            [
                'type' => 'textarea',
                'name' => 'meta_description',
                'text' => 'Мета описание',
                'required' => true,
                'value' => $feast->meta_description
            ],
            [
                'type' => 'text',
                'name' => 'meta_keywords',
                'text' => 'Мета ключевые слова',
                'required' => false,
                'value' => $feast->meta_keywords
            ],
        ];
        return view('admin.layouts.edit_form', [
            'form' => $form,
            'method' => 'put',
            'form_route' => route('admin.feasts.update', $feast->id),
            'delete' => route('admin.feasts.destroy', $feast->id),
            'title' => 'праздника'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feast $feast)
    {
        $request->request->remove('_token');
        $request->request->remove('_method');
        $feast->update($request->request->all());
        FeastDate::updateOrCreate([
            'feast_id' => $feast->id,
        ], [
            'date' => $request->date,
        ]);
        return redirect(route('admin.feasts.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Feast::find($id)->delete();
        return redirect(route('admin.feasts.index'));
    }
}
