<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Feast;
use App\File;
use App\ICategory;
use App\Image;
use App\Postcard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image as Imager;
use IvanLemeshev\Laravel5CyrillicSlug\SlugFacade;

class PostcardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $postcards = Postcard::with('categories')->orderBy('id', 'desc')->paginate(20);
        $rows = [];
        foreach ($postcards as $k => $item) {
            $rows[] = [
                'values' => [
                    $item->thumb_image,
                    $item->name,
                    $item->categoryList(),
                ],
                'link' => route('admin.postcards.edit', $item->id),
            ];
        }
        $table = [
            'columns' => ['Изображение', 'Наименование', 'Категории'],
            'rows' => $rows,
        ];
        return view('admin.layouts.table_image_page', [
            'table' => $table,
            'title' => 'открыток',
            'create' => route('admin.postcards.create'),
            'pagination' => $postcards,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::withoutGlobalScope('no_feast')
            ->has('childrens', 0)
            ->orWhere('is_feast', 1)
            ->with('parent')->get()->keyBy('id');
        $form = [
            [
                'type' => 'text',
                'name' => 'name',
                'text' => 'Название',
                'required' => true,
                'value' => ''
            ],
            [
                'type' => 'image',
                'name' => 'path',
                'text' => 'Изображение',
                'required' => true,
            ],
            [
                'type' => 'select-multiple',
                'name' => 'categories[]',
                'text' => 'Категории',
                'required' => false,
                'value' => [],
                'items' => $categories->sortBy('name'),
                'name_key' => 'path_name'
            ],
        ];
        return view('admin.layouts.edit_form', [
            'form' => $form,
            'method' => 'post',
            'form_route' => route('admin.postcards.store'),
            'title' => 'открытки'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->remove('_token');
        $request->request->remove('_method');
        $cats = $request->categories;
        $request->request->remove('categories');
        if (isset($request->path)) {
            Image::uploadCustom(Imager::make($request->path)
                ->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode('jpg', 80), $request->path, 'postcards/', 'thumb_', ".gif", ".jpg");
            $request->request->set('path', File::find(
                Image::upload($request->path, '/postcards')
            )->path);
        }

        $request->request->set('slug', SlugFacade::make($request->name));
        $postcard = Postcard::create($request->request->all());
        $postcard->categories()->attach($cats);
        $postcard->slug = $postcard->id.'-'.$postcard->slug;
        $postcard->save();
        return redirect(route('admin.postcards.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $postcard = Postcard::with('categories')->findOrFail($id);
        $categories = Category::withoutGlobalScope('no_feast')
            ->has('childrens', 0)
            ->orWhere('is_feast', 1)
            ->with('parent')->get()->keyBy('id');
        $form = [
            [
                'type' => 'text',
                'name' => 'name',
                'text' => 'Название',
                'required' => true,
                'value' => $postcard->name,
            ],
            [
                'type' => 'image',
                'name' => 'path',
                'text' => 'Изображение',
                'required' => true,
                'value' => $postcard->path,
            ],
            [
                'type' => 'select-multiple',
                'name' => 'categories[]',
                'text' => 'Категории',
                'required' => false,
                'value' => $postcard->categories->pluck('id', 'id')->toArray(),
                'items' => $categories->sortBy('name'),
                'name_key' => 'path_name'
            ],
        ];
        return view('admin.layouts.edit_form', [
            'form' => $form,
            'method' => 'put',
            'title' => 'открытки',
            'form_route' => route('admin.postcards.update', $id),
            'delete' => route('admin.postcards.destroy', $id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->request->remove('_token');
        $request->request->remove('_method');
        $cats = $request->categories;
        $request->request->remove('categories');
        if (isset($request->path)) {
            Image::uploadCustom(Imager::make($request->path)
                ->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode('jpg', 80), $request->path, 'postcards/', 'thumb_', ".gif", ".jpg");
            $request->request->set('path', File::find(
                Image::upload($request->path, '/postcards')
            )->path);
        }

        $postcard = Postcard::find($id);
        $postcard->update($request->request->all());
        $postcard->categories()->detach();
        $postcard->categories()->attach($cats);
        return redirect(route('admin.postcards.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Postcard::find($id)->delete();
        return redirect(route('admin.postcards.index'));
    }
}
