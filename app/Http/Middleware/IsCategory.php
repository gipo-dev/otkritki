<?php

namespace App\Http\Middleware;

use Closure;

class IsCategory
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->isPostcard = false;
        return $next($request);
    }
}
