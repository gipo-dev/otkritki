<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class File extends Model
{
    protected $table = 'files';

    public static function upload($file, $addingPath = '')
    {
        $filename = time() . $file->getClientOriginalName();
        Storage::disk('public')->putFileAs(
            'uploads' . $addingPath,
            $file,
            $filename
        );
        $path = '/storage/uploads'.$addingPath.'/'.$filename;
//        $path = '/storage/'.$file->store('uploads'.$addingPath, 'public');
        $file = new File();
        $file->user_id = Auth::user()->id;
        $file->path = $path;
        $file->save();
        return $file->id;
    }
}
