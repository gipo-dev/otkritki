<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getIsSuperAttribute() {
        if($this->role_id == 1)
            return true;
        else
            return false;
    }

    public function getIsModerAttribute() {
        if($this->role_id == 1 || $this->role_id == 2)
            return true;
        else
            return false;
    }

    public function getRoleNameAttribute() {
        if($this->attributes['role_id'] == 1)
            return 'Администратор';
        else if($this->attributes['role_id'] == 2)
            return 'Модератор';
        return 'Не установлена';
    }
}

class UserRole extends Model {

}
