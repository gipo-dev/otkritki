<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;

class Category extends ICategory
{
    protected $attributes = ['is_feast' => false];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('no_feast', function (Builder $builder) {
            $builder->where('is_feast', 0);
        });
    }

    public function randomItem() {
        return $this->belongsToMany(Postcard::class)->inRandomOrder()->limit(10);
    }

    public function childrens() {
        return $this->hasMany(Category::class, 'parent_id');
    }
}

class CategoryBackground extends Model
{
    protected $fillable = ['name', 'path', 'category_id'];

    public function category() {
        return $this->belongsTo(CategoryBackgroundCategory::class, 'category_id');
    }

    public function getNameAttribute() {
        return $this->category->attributes['name'].' -> '.$this->attributes['name'];
    }
}

class CategoryBackgroundCategory extends Model
{
    protected $fillable = ['name'];

    public $timestamps = false;
}
