<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Agent\Facades\Agent;

class Postcard extends Model
{
    protected $fillable = ['name', 'path', 'slug'];

    public function getImageAttribute()
    {
        if (Agent::isMobile()) {
            return $this->thumbImage;
        } else {
            return $this->path;
        }
    }

    public function getLinkAttribute()
    {
        $request = request();
        if(isset($request->isPostcard) && $request->isPostcard) {
            if($request->postcard == null) {
                return route('category.show', [
                    $request->category,
                    $this->slug
                ]);
            } else {
                return route('category.show', [
                    $request->category,
                    $request->postcardOrCategory,
                    $this->slug
                ]);
            }
        }

        if ($request->category != null) {
            if ($request->postcardOrCategory != null && $request->postcard != null)
                return route('category.show', [
                    $request->category,
                    $request->postcardOrCategory,
                    $this->slug
                ]);
            else {
                return $request->url() . '/' . $this->slug;
            }
        } else {
            $category = $this->categories()->first();
            return route('category.show', [$category->parent->slug ?? '', $category->slug ?? '', $this->slug]);
        }
    }

    public function getThumbImageAttribute()
    {
        $image = explode('/', $this->path);
        $image[array_key_last($image)] = 'thumb_' . str_replace('.gif', '.jpg', $image[array_key_last($image)]);
        return implode('/', $image);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class)
            ->withoutGlobalScope('no_feast')
            ->with('parent');
    }

    public function categoryList()
    {
        return implode(', ', $this->categories->pluck('name')->toArray());
    }

    public function popularity()
    {
        return $this->hasOne(PostcardPopularity::class)->where('date', Carbon::now()->format('Y-m-d'));
    }

    public function addView()
    {
        $pop = PostcardPopularity::firstOrNew([
            'postcard_id' => $this->id,
            'date' => Carbon::now()->format('Y-m-d'),
        ]);
        $pop->views++;
        $pop->save();
        return $pop;
    }
}

class PostcardPopularity extends Model
{
    protected $attributes = ['views' => 1];
    protected $fillable = ['postcard_id', 'views', 'date'];

    public function postcard()
    {
        return $this->belongsTo(Postcard::class);
    }
//    public $timestamps = false;
}
