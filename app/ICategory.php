<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

abstract class ICategory extends Model
{
    protected $fillable = ['name', 'parent_id', 'background_id', 'sort', 'slug', 'icon',
        'meta_title', 'meta_description', 'meta_keywords', 'image', 'description'];

    public function parent() {
        return $this->belongsTo(Category::class, 'parent_id');
    }
    public function childs() {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function background() {
        return $this->belongsTo(CategoryBackground::class, 'background_id');
    }

    public static function categoryTree() {
        return Category::all()->groupBy('parent_id');
    }

    public function postcards() {
        return $this->belongsToMany(Postcard::class);
    }

    public function getPathNameAttribute() {
        if($this->parent != null)
            $c_name = '<-'.$this->parent->name;
        else if(!$this->is_feast)
            $c_name = '';
        else
            $c_name = '<- Праздники';
        return $this->name.$c_name;
    }
}
